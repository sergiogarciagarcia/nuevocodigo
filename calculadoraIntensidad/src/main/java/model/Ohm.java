package model;

/**
 * El propósito de esta clase Ohm.java es calcular las
 * tres variables necesarias para la ejecución de la clase calculadora.java;
 * voltaje, resistencia e intensidad.
 *
 *
 * @author Usuario
 */
public class Ohm {

    Double voltaje;
    Double resistencia;
    Double intensidad;

    /**
     * 
     *
     * El propósito del método calVoltaje, es el de calcular precisamente el
     * voltaje en una multiplicación a partir de dos parámetros resistencia e
     * intensidad. El método retorna el voltaje.
     *
     * @param resistencia1 valor de la resistencia ingresada por el usuario
     * @param intensidad1 valor de la intensidad ingresada por el usuario
     * @return Retorna el calculo de el voltaje
     */
    public Double calVoltaje(Double resistencia1, Double intensidad1) {
        this.voltaje = resistencia1 * intensidad1;
        return voltaje;
    }

    /**
     * 
     * El propósito del método calResistencia, es el de calcular precisamente la
     * resistencia en una división a partir de dos parámetros voltaje e
     * intensidad. El método retorna la resistencia.
     *
     * @param voltaje1 valor de el voltaje ingresada por el usuario
     * @param intensidad1 valor de la intensidad ingresada por el usuario
     * @return Retorna el calculo de la resistencia
     */
    public Double calResistencia(Double voltaje1, Double intensidad1) {

        this.resistencia = voltaje1 / intensidad1;
        return resistencia;
    }

    /**
     * 
     * El propósito del método calIntensidad, es el de calcular precisamente la
     * intensidad en una división a partir de dos parámetros resistencia y
     * voltaje. El método retorna la intensidad.
     *
     * @param resistencia1 valor de la resistencia ingresada por el usuario
     * @param voltaje1 valor de el voltaje ingresada por el usuario
     * @return Retorna el calculo de la intensidad
     */
    public Double calIntensidad(Double resistencia1, Double voltaje1) {

        this.intensidad = voltaje1 / resistencia1;
        return intensidad;
    }

}

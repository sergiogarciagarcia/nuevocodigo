/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Usuario
 */
public class OhmTest {
    
    public OhmTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    

    /**
     * Test of calVoltaje method, of class Ohm.
     */
    
    
    @org.junit.jupiter.api.Test
    public void testCalVoltaje1() {
        System.out.println("El voltaje es cero");
        Double resistencia1 = 0.0;
        Double intensidad1 = 5.0;
        Ohm instance = new Ohm();
        Double expResult = 0.0;
        Double result = instance.calVoltaje(resistencia1, intensidad1);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        /*fail("The test case is a prototype.");*/
    }
    
    @org.junit.jupiter.api.Test
    public void testCalVoltaje2() {
        System.out.println("El voltaje es negativo");
        Double resistencia1 = -1.0;
        Double intensidad1 = 2.0;
        Ohm instance = new Ohm();
        Double expResult = -2.0;
        Double result = instance.calVoltaje(resistencia1, intensidad1);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        /*fail("The test case is a prototype.");*/
    }
    
    @org.junit.jupiter.api.Test
    public void testCalVoltaje3() {
        System.out.println("El voltaje es positivo");
        Double resistencia1 = -3.0;
        Double intensidad1 = -10.0;
        Ohm instance = new Ohm();
        Double expResult = 30.0;
        Double result = instance.calVoltaje(resistencia1, intensidad1);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        /*fail("The test case is a prototype.");*/
    }

    

    
    
}
